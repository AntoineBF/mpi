# MPI exercices

*course exercises by **David Henty** at **the University of Edinburgh** and **EPCC**.*

*[The link to the source](https://www.archer2.ac.uk/training/courses/220323-mpi/)*

```
What is MPI?

It is the Message-Passing Interface, a way of doing distributed-memory parallel programming.

It enables independent programms subsequently to communicate with each other.
```

## TABLE OF EXERCICES

| Exercices | Status |
|:---|:---|
| [Hello World](#hello-world) | :heavy_check_mark: |
| [Parallel calculation of $\pi$](#parallel-calculation-of) | :heavy_check_mark: |
| [Broadcast and Scatter](#broadcast-and-scatter) | :heavy_check_mark: |
| [Ping Pong](#ping-pong) | :heavy_check_mark: |
| [Rotating information around a ring](#rotating-information-around-a-ring) | :heavy_check_mark: |
| [Collective communications](#collective-communications) | :heavy_check_mark: |
| [Rotating information using a Cartesian topology](#rotating-information-using-a-cartesian-topology) | :heavy_check_mark: |
| [Derived Datatypes](#derived-datatypes) | :heavy_minus_sign: |

Legend:

+ :heavy_check_mark: : finish

+ :heavy_minus_sign: : unfinish

+ :x: : not available


## [Hello World](/helloworld.c)

1. Write an MPI program which prints the message **Hello World**.

2. Modify your program so that each process prints out both its rank and the total number of processes P that the code is running on, i.e. the size of `MPI_COMM_WORLD`.

3. Modify your program so that only a single controller process (e.g. rank 0) prints out a message (very useful when you run with hundreds of processes).

4. What happens if you omit the final MPI procedure call in your program?

```
It is essential to include MPI_Finalize() at the end of your MPI code to ensure proper termination, resource deallocation, and to avoid any potential issues.
```

## [Parallel calculation of $\pi$](/pi.c)

An approximation to the value π can be obtained from the following expression

$\frac{\pi}{4}=\int_{0}^1{\frac{dx}{1+x^2}}\approx\frac{1}{N}\sum_{i=1}^N\frac{1}{1+(\frac{i-\frac{1}{2}}{N})^2}$

where the answer becomes more accurate with increasing N . Iterations over i are independent so the calculation can be parallelised.

For the following exercises you should set N = 840. This number is divisible by 2, 3, 4, 5, 6, 7 and 8 which is convenient when you parallelise the calculation!

1. Modify your Hello World program so that each process independently computes the value of $\pi$ and prints it to the screen. Check that the values are correct (each process should print the same value).

2. Now arrange for different processes to do the computation for different ranges of i. For example,
on two processes: rank 0 would do $i = 1, 2, . . . , \frac{N}{2}$ ; rank 1 would do $i = \frac{N}{2} + 1, \frac{N}{2} + 2, . . . , N$.

Print the partial sums to the screen and check the values are correct by adding them up by hand.

3. Now we want to accumulate these partial sums by sending them to the controller (e.g. rank 0) to add up:
    * all processes (except the controller) send their partial sum to the controller.
    * the controller receives the values from all the other processes, adding them to its own partial sum.

You should use the MPI routines `MPI_Ssend` and `MPI_Recv`.

4. Use the function MPI_Wtime (see below) to record the time it takes to perform the calculation.

For a given value of N, does the time decrease as you increase the number of processes? Note that to ensure that the calculation takes a sensible amount of time (e.g. more than a second) you will probably have to perform the calculation of π several thousands of times.

5. Ensure your program works correctly if N is not an exact multiple of the number of processes P.

### Timing MPI Programs

The `MPI_Wtime()` routine returns a double-precision floating-point number which represents elapsed wall-clock time in seconds. The timer has no defined starting-point, so in order to time a piece of code, two calls are needed and the difference should be taken between them. There are a number of important considerations when timing a parallel program:

1. Due to system variability, it is not possible to accurately time any program that runs for a very short time. A rule of thumb is that you cannot trust any measurement much less than one second.

2. To ensure a reasonable runtime, you will probably have to repeat the calculation many times within a do/for loop. Make sure that you remove any print statements from within the loop, otherwise there will be far too much output and you will simply be measuring the time taken to print to screen.

3. Due to the SPMD nature of MPI, each process will report a different time as they are all running independently. A simple way to avoid confusion is to synchronise all processes when timing, e.g.

```C
MPI_Barrier(MPI_COMM_WORLD);    // Line up at the start line
tstart = MPI_Wtime();           // Start the clock
MPI_Barrier(MPI_COMM_WORLD);    // Wait for everyone to finish
tstop = MPI_Wtime();            // Stop the clock
```

Note that the barrier is only needed to get consistent timings – it should not affect code correctness.

With synchronisation in place, all processes will record roughly the same time (the time of the slowest process) and you only need to print it out on a single process (e.g. rank 0).

## [Broadcast and Scatter](/bcast.c)

You should write a simple program using send and receive calls to implement broadcast and scatter operations. We will see later in the course how to do this using MPI’s built-in collectives, but it is useful to implement them by hand as they illustrate how to communicate all or part of an array of data using point-to-point operations rather than just a single variable (as illustrated by the pi example).

Write an MPI program to do the following, using `MPI_Ssend` for all send operations.

1. declare an integer array x of size N (e.g. N = 12)

2. initialise the array
    * on rank 0: xi = i
    * on other ranks: xi = −1

3. print out the initial values of the arrays on all processes

4. implement the broadcast:
    * on rank 0: send the entire array x to all other processes
    * on other ranks: receive the entire array from rank 0 into x

5. print out the final values of the arrays on all processes

Now extend your program to implement scatter:

1. declare and initialise the array x as before

2. for P processes, imagine that the array x on rank 0 is divided into P sections, each of size N/P

3. implement the scatter:
    * on rank 0: send the ith section of x to rank i
    * other ranks: receive an array of size N/P from rank 0 into x

4. print out the final values of the arrays on all processes

To ensure that output from separate processes is not interleaved, you can use this piece of code for printing  the arrays. To print 10 elements from an array x: printarray(rank, x, 10);

```C
void printarray(int rank, int *array, int n) {
  int i;
  printf("On rank %d, array[] = [", rank);
  for (i = 0; i < n; i++) {
    if (i != 0)
      printf(",");
    printf(" %d", array[i]);
  }
  printf(" ]\n");
}
```

## [Ping Pong](/pingpong.c)

1. Write a program in which two processes (say rank `0` and rank `1`) repeatedly pass a message back and forth. Use the synchronous mode `MPI_Ssend` to send the data. You should write your program so that it operates correctly even when run on more than two processes, i.e. processes with rank greater than one should simply do nothing. For simplicity, use a message that is an array of integers. Remember that this is like a game of table-tennis:
  * rank `0` should send a message to rank `1`
  * rank `1` should receive this message then send the same data back to rank `0`
  * rank `0` should receive the message from rank `1` and then return it
  * etc. etc.

2. Insert timing calls to measure the time taken by all the communications. You will need to time many ping-pong iterations to get a reasonable elapsed time, especially for small message lengths.

3. Investigate how the time taken varies with the size of the message. You should fill in your results in the table below. What is the asymptotic bandwidth for large messages?

4. Plot a graph of time against message size to determine the latency (i.e. the time taken for a message of zero length); plot a graph of the bandwidth to see how this varies with message size. 

The bandwidth and latency are key characteristics of any parallel machine, so it is always instructive to run this ping-pong code on any new computers you may get access to.

| Size (bytes) | # Iteractions | Total time (secs) | Time per message | Bandwidth (MB/s) |
|:---|:---|:---|:---|:---|

## [Rotating information around a ring](/rotate.c)

![fig1](./img/fig1.png)

Consider a set of processes arranged in a ring as shown in Figure 1. A simple way to perform a global sum of data stored on each process (a parallel reduction operation) is to rotate each piece of data all the way round the ring. At each iteration, a process receives some data from the left, adds the value to its running total, then passes the data it has just received on to the right.

Figure 1 illustrates how this works for four processes (ranks 0, 1, 2 and 3) who hold values $A$, $B$, $C$ and $D$ respectively. The running total on each process is shown in the square box, and the data being sent between processes is shown next to the arrow. After three steps ($P − 1$ steps in general for $P$ processes) each process has computed the global sum $A + B + C + D$.

1. Write a program to perform a global sum using this simple ring method. Each process needs to know the ranks of its two neighbours in the ring, which stay constant throughout the program. You should use synchronous sends and avoid deadlock by using non-blocking forms for either the send (`MPI_Issend`) **or** the receive (`MPI_Irecv`). 

Remember that you cannot assume that a non- blocking operation has completed until you have issued an explicit wait. You can use non-blocking calls for send and receive, but you will have to store two separate requests and wait on them both. We need to initialise the local variables on each process with some process-dependent value. 

For simplicity, we will just use the value of rank, i.e. in Figure 1 this would mean $A = 0$, $B = 1$, $C = 2$ and $D = 3$. You should check that every process computes the sum correctly (e.g. print the final value to the screen), which in this case is $P(P − 1)/2$.

2. Your program should compute the correct global sum for any set of input values. If you initialise the local values to $(rank + 1)^2$, do you get the correct result $P(P + 1)(2P + 1)/6$ ?

### Timing

1. Measure the time taken for a global sum and investigate how it varies with increasing $P$. Plot a graph of time against $P$ — does the ring method scale as you would expect?

2. Using these timings, estimate how long it takes to send each individual message between processes. How does this result compare with the latency figures from the ping-pong exercise?

### Avoid deadlock

1. The `MPI_Sendrecv` call is designed to **avoid deadlock** by combining the separate send and receive operations into a single routine. Write a new version of the global sum using this routine and compare the time taken with the previous implementation. Which is faster?

2. Investigate the time taken when you use standard and buffered sends rather than synchronous mode (using `MPI_Bsend` you do not even need to use the non-blocking form as it is guaranteed to be asynchronous). Which is the fastest? By comparing to the time taken by the combined send and receive operation, can you guess how `MPI_Sendrecv` is actually being implemented?

| MPI_Issend | MPI_Sendrecv |
|:---|:---|
| 0.000133 | 0.000101 |

## [Collective communications](./coll_comm.c)

1. Re-write the ring example using an MPI reduction operation to perform the global sum.

2. How does execution time vary with $P$ and how does this compare to your own ring method?

| MPI_Issend | MPI_Sendrecv | MPI_Allreduce |
|:---|:---|:---|
| 0.000133 | 0.000101 | 0.000096 |

### Performance

1. Compare the performance of a single reduction of an array of $N$ values compared with $N$ separate calls to reductions of a single value. Can you explain the results (the latency and bandwidth values from the pingpong code are useful to know)?

| a single reduction of an array of $N$ values | $N$ separate calls to reductions of a single value |
|:---|:---|
| 0.000096 | 0.000142 |

2. You can ensure all processes get the answer of a reduction by doing MPI_Reduce followed by `MPI_Bcast`, or using `MPI_Allreduce`. Compare the performance of these two methods.

| MPI_Issend | MPI_Sendrecv | MPI_Allreduce | MPI_Bcast |
|:---|:---|:---|:---|
| 0.000133 | 0.000101 | 0.000096 | 0.000084 |

3. Imagine you want all the processes to write their output, in order, to a single file. The important point is that only one process can have the file open at any given time. Using the MPI_Barrier routine, modify your code so each process in turn opens, appends to, then closes the output file.

## [Rotating information using a Cartesian topology](/cart.c)

For a 1D arrangement of processes it may seem a lot of effort to use a Cartesian topology rather than managing the processes by hand, e.g. calculating the ranks of the nearest neighbours. It is, however, worth learning how to use topologies as these book-keeping calculations become tedious to do by hand in two and three dimensions. For simplicity we will only use the routines in one dimension. Even for this simple case the exercise shows how easy it is to change the boundary conditions when using topologies.

1. Re-write the passing-around-a-ring exercise so that it uses a one-dimensional Cartesian topology, computing the ranks of the nearest neighbours using `MPI_Cart_shift`. Remember to set the periodicity of the boundary conditions appropriately for a ring rather than a line.

```C
On process 2: sum=5 (0.000863)
On process 3: sum=14 (0.000864)
On process 0: sum=0 (0.000872)
On process 1: sum=1 (0.000862)
```

2. Alter the boundary conditions to change the topology from a ring to a line, and re-run your program. Be sure to run using both of the initial values, i.e. rank and $(rank + 1)^2$ . Do you understand the output? What reduction operation is now being implemented? What are the neighbouring ranks for the two processes at the extreme ends of the line?

```C
[-1;0;1]
[0;1;2]
On process 1: sum=5 (0.000127)
[1;2;3]
On process 2: sum=14 (0.000127)
[2;3;0]
On process 3: sum=30 (0.000122)
On process 0: sum=1 (0.000127)
```

3. Check the results agree with those obtained from calling an `MPI_Scan` collective routine.

```C
On process 0: sum=1 (0.000087)
On process 1: sum=5 (0.000089)
On process 2: sum=14 (0.000089)
On process 3: sum=30 (0.000089)
```

## [Derived Datatypes](/der_data.c)

We will extend exercise 5 to perform a global sum of a non-basic datatype. A simple example of this is a compound type containing both an integer and a double-precision floating-point number. Such a compound type can be declared as a structure in C as follows:

```C
typedef struct compound {
  int ival;
  double dval;
} Compound;

Compound sum, add, pass;

pass.ival = rank;
pass.dval = pow(rank+1, 2);
```

If you are unfamiliar with using derived types in Fortran then I recommend that you go straight to exercise number 2 which deals with defining MPI datatypes to map onto subsections of arrays. This is, in fact, the most common use of derived types in scientific applications of MPI.

1. Modify the ring exercise so that it uses an `MPI_Type_struct` derived datatype to pass round the above compound type, and computes separate integer and floating-point totals. You will need to use MPI_Address to obtain the displacements of ival and dval. Initialise the integer part to rank and the floating-point part to $(rank + 1)^2$ and check that you obtain the correct results.

2. Modify your existing ping-pong code to exchange N x N square matrices between the processes (`int msg[N][N]` in C). Initialise the matrix elements to be equal to rank so that you can keep track of the messages. Define `MPI_Type_contiguous` and `MPI_Type_vector` derived types to represent N x M (type mcols) and M x N (type mrows) subsections of the matrix, where $M \leq N$ . Which datatype to use for each subsection depends on whether you are using C. You may find it helpful to refer to Figure 2 to clarify this, where I draw the arrays in the standard fashion for matrices (indices start from one rather than zero, first index goes down, second index goes across). Set $N = 10$ and $M = 3$ and exchange columns 4, 5 and 6 of the matrix using the `mcols` type. Print the entire matrix on each process after every stage of the ping-pong to check that for correctness. 
Now modify your program to exchange rows 4, 5 and 6 using the mrows type.

## Notice

* :warning: Timings only make sense when no print statements are includes.