#include <stdio.h>
#include <mpi.h>

void printarray(int rank, int *array, int n);

int main(void) {
  int i, src, dest, rank, size, root, tag;
  MPI_Comm comm = MPI_COMM_WORLD;
  MPI_Status status;

  // Declare the size of array (1)
  const int N = 12;

  int x[N];

  root = 0; // The process that contains the data to be broadcast
  tag = 0;

  MPI_Init(NULL, NULL);
  MPI_Comm_rank(comm, &rank);   // return in rank the rank of the calling process in comm.
  MPI_Comm_size(comm, &size);   // return in size the number of process in comm.
  
/*
  if (rank == 0)  printf("Running broadcast program on %d processes\n", size);

  // Initialization of array (2)
  for (i = 0; i < N; i++) {
    if (rank == root)
      x[i] = i;
    else
      x[i] = -1;
  }

  // Print out the initial values (3)
  printarray(rank, x, N);

  // We are waiting everyone 
  MPI_Barrier(MPI_COMM_WORLD);

  // Implement the broadcast (4)
  if (rank == root) {
    printf("Sending...\n");
    for (dest = 0; dest < size; dest++) {
      if (dest != root)  MPI_Ssend(&x, N, MPI_INT, dest, tag, comm);
    }
  } else
    MPI_Recv(&x, N, MPI_INT, root, tag, comm, &status);

  // Print out the final values (5)
  printarray(rank, x, N);

*/
  // Now, it's time for a scatter
  if (rank == 0)  printf("Running scatter program on %d processes\n", size);

  // Initialization of array (1)
  for (i = 0; i < N; i++) {
    if (rank == root)
      x[i] = i;
    else
      x[i] = -1;
  }

  // Print out the initial values
  printarray(rank, x, N);

  // We are waiting everyone 
  MPI_Barrier(MPI_COMM_WORLD);

  int size_part = N/size;

  // Implement the scatter (3)
  if (rank == root) {
    printf("Sending...\n");
    for (dest = 0; dest < size; dest++) {
      if (dest != root)  MPI_Ssend(&x[dest*size_part], size_part, MPI_INT, dest, tag, comm);
    }
  } else
    MPI_Recv(&x[rank*size_part], size_part, MPI_INT, root, tag, comm, &status);
  
  // Print out the final values (4)
  printarray(rank, x, N);

  MPI_Finalize();

  return 0;
}

void printarray(int rank, int *array, int n) {
  int i;
  printf("On rank %d, array[] = [", rank);
  for (i = 0; i < n; i++) {
    if (i != 0)
      printf(",");
    printf(" %d", array[i]);
  }
  printf(" ]\n");
}

/*
// Non-fruiting test

int y[size];
  for(int i = 0; i < size; i++)  
    if(i < N%size)  y[i] = 1;
    else  y[i] = 0;

  int size_part = N/size;

  for(int i = 0; i < size; i++)
    printf("y[i]=%d\n", y[i]);
  printf("\n");

  int tmp = 0;
  int tmpRecv = 0;
  // Implement the scatter (3)
  if (rank == root) {
    printf("Sending...\n");
    for (dest = 0; dest < size; dest++) {
      if (dest != root) {
        if(y[dest] != 0)  tmp++;
        MPI_Ssend(&x[dest*size_part+tmp], size_part+y[dest], MPI_INT, dest, tag, comm);
        for (int j = dest*size_part+tmp; j < (dest+1)*size_part+y[dest]+tmp; j++) {
          x[j] = -1;
        }
      }
    }
  } else {
    if(y[rank] != 0)  tmpRecv++;
    MPI_Recv(&x[rank*size_part+y[rank-1]*(rank+1)-tmpRecv], size_part+y[rank]*(rank+1), MPI_INT, root, tag, comm, &status);
  }
*/