#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <mpi.h>

#define ndims 1    

int main(void) {
  MPI_Comm comm = MPI_COMM_WORLD;
  MPI_Status status;

  int rank, size;
  int left, right;

  double tstart, tstop, time;

  int sum, add, pass;

  MPI_Request request;
  MPI_Comm comm1d;
  int direction,disp;
  int dims[ndims];
  int periods[ndims];
  int reorder;

  MPI_Init(NULL, NULL);
  MPI_Comm_rank(comm, &rank);   // return in rank the rank of the calling process in comm.
  MPI_Comm_size(comm, &size);   // return in size the number of process in comm.

  left = (rank - 1)%4;
  right = (rank + 1)%4;

  MPI_Barrier(comm);    // Line up at the start line
  tstart = MPI_Wtime(); // Start the clock

  dims[0] = 0;
  periods[0] = 0; //true -> cyclic
  reorder = 0;    //false
  direction = 0;  // for the first dimension
  disp = 1;       // upwards shift of 1
/*
  MPI_Dims_create(size,ndims,dims);
  MPI_Cart_create(comm,ndims,dims,periods,reorder,&comm1d);
  MPI_Comm_rank(comm1d,&rank);
  MPI_Cart_shift(comm1d,direction,disp,&left,&right);
*/
  sum = 0;

  //pass = rank;
  pass = pow(rank+1, 2);
/*
  for(int i = 0; i < size; i++) {
    MPI_Issend(&pass, 1, MPI_INT, right, 0, comm, &request);
    MPI_Recv(&add, 1, MPI_INT, left, 0, comm, &status);
    MPI_Wait(&request, &status);
    sum += add; // adds received value to sum
    pass = add; // assigns the pass value with the add value
  }*/
/*
  for(int i = 0; i < size; i++) {
    MPI_Sendrecv(&pass, 1, MPI_INT, right, 0, &add, 1, MPI_INT, left, 0, comm1d, &status);
    //MPI_Wait(&request, &status);
    sum += add; // adds received value to sum
    pass = add; // assigns the pass value with the add value
  }*/

  // MPI_Allreduce(&pass,&sum,1,MPI_INT,MPI_SUM,comm1d);
  //MPI_Reduce(&pass,&sum,1,MPI_INT,MPI_SUM, 0,comm);
  //MPI_Bcast(&sum,1,MPI_INT,root,comm);

  printf("[%d;%d;%d]\n", left, rank, right);

  MPI_Scan(&pass,&sum,1,MPI_INT,MPI_SUM,comm);

  MPI_Barrier(comm);    // Wait for everyone to finish
  tstop = MPI_Wtime();  // Stop the clock

  time = tstop-tstart;

  printf("On process %d: sum=%d (%f)\n", rank, sum, time);

  MPI_Finalize();

  return 0;
}