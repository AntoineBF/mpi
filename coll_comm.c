#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <mpi.h>

#define N 840

int main(void) {
  MPI_Comm comm = MPI_COMM_WORLD;
  MPI_Status status;

  int rank, size;
  int left, right;

  double tstart, tstop, time;

  int sum, add, pass;
  int root = 0;

  MPI_Request request;

  MPI_Init(NULL, NULL);
  MPI_Comm_rank(comm, &rank);   // return in rank the rank of the calling process in comm.
  MPI_Comm_size(comm, &size);   // return in size the number of process in comm.

  MPI_Barrier(comm);    // Line up at the start line
  tstart = MPI_Wtime(); // Start the clock
/*
  left = (rank - 1)%4;
  right = (rank + 1)%4;
*/
  sum = 0;

  //pass = rank;
  pass = pow(rank+1, 2);
/*
  for(int i = 0; i < size; i++) {
    MPI_Issend(&pass, 1, MPI_INT, right, 0, comm, &request);
    MPI_Recv(&add, 1, MPI_INT, left, 0, comm, &status);
    MPI_Wait(&request, &status);
    sum += add; // add received value to sum
    pass = add; // assigns the pass value with the add value
  }*/
/*
  // Avoid deadlock
  for(int i = 0; i < size; i++) {
    MPI_Sendrecv(&pass, 1, MPI_INT, right, 0, &add, 1, MPI_INT, left, 0, comm, &status);
    //MPI_Wait(&request, &status);
    sum += add; // add received value to sum
    pass = add; // assigns the pass value with the add value
  }
*/
  // Collective communications
  
  //MPI_Allreduce(&pass,&sum,1,MPI_INT,MPI_SUM,comm);
/*
  for(int i = 0; i < size; i++)
    MPI_Reduce(&pass, &sum, 1, MPI_INT, MPI_SUM, i, comm);
*/

  MPI_Reduce(&pass, &sum, 1, MPI_INT, MPI_SUM, root, comm);
  MPI_Bcast(&sum,1,MPI_INT,root,comm);

  MPI_Barrier(comm);    // Wait for everyone to finish
  tstop = MPI_Wtime();  // Stop the clock

  time = tstop-tstart;

  printf("On process %d: sum=%d (%f)\n", rank, sum, time);

  // Open the file in append mode
  FILE* file = fopen("example.txt", "a");

  if (file == NULL) {
    printf("Failed to open the file.\n");
    return 1;
  }

  // Write some data to the file
  fprintf(file, "rank: %d\n", rank);

  // Close the file
  fclose(file);

  printf("File modified and closed successfully.\n");

  MPI_Finalize();

  return 0;
}
