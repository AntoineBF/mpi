#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <mpi.h>

typedef struct compound {
  int ival;
  double dval;
} Compound;

void init(Compound *str);

#define N 840

int main(void) {
  Compound sum, add, pass;

  MPI_Status status;
  MPI_Comm comm = MPI_COMM_WORLD;
  MPI_Request request;

  int rank, size;
  int left, right;
  double tstart, tstop, time;

  int length[2] = {1,1};
  MPI_Aint disp[2],base;
  MPI_Datatype type[2], newtype;
  
  MPI_Init(NULL, NULL);
  MPI_Comm_rank(comm, &rank);   // return in rank the rank of the calling process in comm.
  MPI_Comm_size(comm, &size);   // return in size the number of process in comm.

  if (size == 1) {
    printf("Must be run on more than 1 processor.\n");
    MPI_Finalize();
    exit(1);
  }

  MPI_Barrier(comm);    // Line up at the start line
  tstart = MPI_Wtime(); // Start the clock

  MPI_Get_address(&pass.ival, &disp[0]);
  MPI_Get_address(&pass.dval, &disp[1]);

  base = disp[0];
  disp[0] -= base;
  disp[1] -= base;

  //disp[0] = (MPI_Aint)offsetof(struct compound, ival);
  //disp[1] = (MPI_Aint)offsetof(struct compound, dval); 

  type[0] = MPI_INT;
  type[1] = MPI_DOUBLE;

  // Create the new datatype, called 'newtype' and commit it
  MPI_Type_create_struct(2,length,disp,type,&newtype);
  MPI_Type_commit(&newtype);

  left = (rank - 1)%4;
  right = (rank + 1)%4;

  init(&sum);
  init(&add);
  init(&pass);

  pass.ival = rank;
  pass.dval = pow(rank+1, 2);

  for(int i = 0; i < size; i++) {
    MPI_Issend(&pass, 1, newtype, right, 1, comm, &request);
    MPI_Recv(&add, 1, newtype, left, 1, comm, &status);
    MPI_Wait(&request, &status);
    
    sum.ival += add.ival; // adds received ivalue to sum
    sum.dval += add.dval; // adds received dvalue to sum
    pass.ival = add.ival; // assigns the pass ivalue with the add ivalue
    pass.dval = add.dval; // assigns the pass dvalue with the add dvalue
  }
/*
  for(int i = 0; i < size; i++) {
    //MPI_Sendrecv(&pass.ival, 1, MPI_INT, right, 0, &add.ival, 1, MPI_INT, left, 0, comm, &status);
    //MPI_Sendrecv(&pass.dval, 1, MPI_DOUBLE, right, 0, &add.dval, 1, MPI_DOUBLE, left, 0, comm, &status);
    MPI_Sendrecv(&pass, 1, newtype, right, 0, &add, 1, newtype, left, 0, comm, &status);
    
    sum.ival += add.ival; // adds received ivalue to sum
    sum.dval += add.dval; // adds received dvalue to sum
    pass.ival = add.ival; // assigns the pass ivalue with the add ivalue
    pass.dval = add.dval; // assigns the pass dvalue with the add dvalue
  }*/

  //MPI_Allreduce(&pass.ival,&sum.ival,1,type[0],MPI_SUM,comm);
  //MPI_Allreduce(&pass.dval,&sum.dval,1,type[1],MPI_SUM,comm);

  MPI_Barrier(comm);    // Wait for everyone to finish
  tstop = MPI_Wtime();  // Stop the clock

  time = tstop-tstart;

  printf("On process %d: sum=[%d;%f] (%f)\n", rank, sum.ival, sum.dval, time);

  MPI_Finalize();

  return 0;
}

void init(Compound *str){
  str->ival = 0;
  str->dval = 0;
}
