#include <stdio.h>
#include <stdlib.h>

#include <mpi.h>

int main(){
  int rank, size;
  MPI_Comm comm = MPI_COMM_WORLD;   // communicator

  MPI_Init(NULL,NULL);
  MPI_Comm_rank(comm, &rank);   // return in rank the rank of the calling process in comm.
  MPI_Comm_size(comm, &size);   // return in size the number of process in comm.

  printf("Among %d process, I am the %dth processes.\n", size, rank);

  // Only processor 0 prints 
  if(rank == 0)  printf("Hello World!: size=%d; rank=%d\n", size, rank);

  MPI_Finalize();

  return 0;
}
