#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <mpi.h>

#define N 840

int main(void) {
  double pi, res_pi;
  int i;

  MPI_Status status;
  MPI_Comm comm = MPI_COMM_WORLD;   // communicator

  int rank, size, source;

  int istart, istop;
  double tstart, tstop, time;
  double tmp_pi, recvpi;

  int nameSize;
  char name[MPI_MAX_PROCESSOR_NAME];

  MPI_Init(NULL, NULL);
  MPI_Comm_rank(comm, &rank);   // return in rank the rank of the calling process in comm.
  MPI_Comm_size(comm, &size);   // return in size the number of process in comm.

/*    EXO 1 
  tmp_pi = 0.0;
  for (i = 0; i < N-1; i++) {
    tmp_pi += 1.0/( 1.0 + pow(((double) i-0.5)/((double) N), 2));
    //printf("tmp_pi:%f\n", tmp_pi);
  }

  pi = (4/((double) N))*tmp_pi;
  printf("Process %d: pi = %f\n", rank, pi);
*/

  MPI_Get_processor_name(name, &nameSize);
  printf("Process %d on %s\n", rank, name);

  MPI_Barrier(comm);    // Line up at the start line
  tstart = MPI_Wtime(); // Start the clock

  if(rank == 0){
    printf("Using N = %d,\n", N);
    printf("On %d process(es)\n", size);
  }

  tmp_pi = 0.0;

  istart = N/size*rank + 1;
  istop  = N/size*(rank+1) + 1;

  /**
   * @brief This code block is calculating the value of pi using the Monte Carlo method in parallel using MPI.
   * 
   * The loop calculates the partial sum of the series for pi for a specific range of values of i
   * assigned to each process based on its rank. The value of pi is then calculated by summing up the
   * partial sums obtained from each process using MPI communication functions.
   * 
   * Finally, the calculated value of pi is compared with the actual value of pi (res_pi) 
   * to calculate the error and print the result.
   */
  for (i=istart; i < istop; i++)
    tmp_pi += 1.0/( 1.0 + pow( (((double) i)-0.5)/((double) N), 2) );

  //printf("Hello World!: size=%d; rank=%d; tmp_pi = %f\n", size, rank, tmp_pi*4.0/((double) N));

  if (rank == 0) {
    pi = tmp_pi;
    for (source = 1; source < size; source++) {
      MPI_Recv(&tmp_pi, 1, MPI_DOUBLE, MPI_ANY_SOURCE, 0, comm, &status);
      pi += tmp_pi;
    }
  } else
    MPI_Ssend(&tmp_pi, 1, MPI_DOUBLE, 0, 0, comm);

  pi *= 4.0/((double) N);

  res_pi = 4.0*atan(1.0);
  
  if (rank == 0)  printf("pi = %f => error = %f\n", pi, fabs(100.0*(pi-res_pi)/res_pi));

  MPI_Barrier(comm);    // Wait for everyone to finish
  tstop = MPI_Wtime();  // Stop the clock

  time = tstop-tstart;

/**
 * @brief This code block is used to gather the execution time of 
 * each process and calculate the total execution time.
 */
  if (rank == 0) {
    double tmp_time;
    for (source = 1; source < size; source++) {
      MPI_Recv(&tmp_time, 1, MPI_DOUBLE, source, 0, comm, &status);
      time += tmp_time;
    }
    printf("time for N=%d with %d process(es): %f\n", N, size, time);
  } else
    MPI_Ssend(&time, 1, MPI_DOUBLE, 0, 0, comm);

  MPI_Finalize();

  return 0;
}
