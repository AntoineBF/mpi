#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <mpi.h>

int main(int argc, char* argv[]){
  MPI_Comm comm = MPI_COMM_WORLD;
  MPI_Status status;

  int rank, size, numiter,length;
  double *sbuffer;

  double tstart, tstop, time, tottime, totmess;
  int type_size; 

  MPI_Request request;

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(comm, &rank);   // return in rank the rank of the calling process in comm.
  MPI_Comm_size(comm, &size);   // return in size the number of process in comm.

  if(size < 2){
    if(rank == 0)  printf("The code must be run on at least 2 processors.\n");
    MPI_Finalize();
    exit(1);
  }

  if(argc < 2) {
    if(rank == 0)  printf("Code requires 2 input arguments: The array length and the number of iterations. \n");
    MPI_Finalize();
    exit(1);
  }

  if(rank >= 2)  printf( "Rank %d not participating \n",rank);
  int tag1 = 1;
  int tag2 = 2;
  
  if (rank == 0) {
    length = atoi(argv[1]);
    numiter = atoi(argv[2]);
    printf("Array length = %d , number of iterations = %d\n",length,numiter);
  }

  MPI_Bcast(&length,1,MPI_INT,0,comm);
  MPI_Bcast(&numiter,1,MPI_INT,0,comm);

  sbuffer = malloc(length*sizeof(double));

  for(int i=0;i<length;i++)
    sbuffer[i] = (double)rank + 10.0;

  MPI_Barrier(comm);    // Line up at the start line
  tstart = MPI_Wtime(); // Start the clock

  for(int i = 1; i < numiter; i++){
    if(rank == 0){
      MPI_Ssend(&sbuffer[0],length,MPI_DOUBLE,1,tag1,comm);
      MPI_Recv(&sbuffer[0],length,MPI_DOUBLE,1,tag2,comm,&status);
    } else if(rank == 1){
      MPI_Recv(&sbuffer[0],length,MPI_DOUBLE,0,tag1,comm,&status);
      MPI_Ssend(&sbuffer[0],length,MPI_DOUBLE,0,tag2,comm);
    }
  }

  MPI_Barrier(comm);    // Wait for everyone to finish
  tstop = MPI_Wtime();  // Stop the clock

  time = tstop-tstart;

  printf("On process %d: time: %f\n", rank, time);

  // Determine size of MPI_DOUBLE for next calculs
  MPI_Type_size(MPI_DOUBLE,&type_size);

  MPI_Allreduce(&time,&tottime,1,MPI_DOUBLE,MPI_SUM,comm);

  if(rank == 0){
    totmess = 2.0*type_size*length*numiter;
    printf("Size (bytes): %d\n", length);
    printf("# Iteractions: %d\n", numiter);
    printf( "Total message size: %f [MB] \n", totmess);
    printf("Total time (secs): %f [s]\n", tottime);
    printf("Time per message (Latency): %f [s]\n",  0.5*(time)/numiter);
    printf( "Bandwidth (message size per time): %f [MB/s]\n",  totmess/(time));
  }

  free(sbuffer);

  MPI_Finalize();

  return 0;
}
